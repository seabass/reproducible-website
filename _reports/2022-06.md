---
layout: report
year: "2022"
month: "06"
title: "Reproducible Builds in June 2022"
draft: true
---

* Vagrant Cascadian updated the *diffoscope* package in [GNU Guix](https://www.gnu.org/software/guix/).&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=1d7222590361ecb0ff56b42872ca6e5754732d08)][[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=2900031f42ba85bebeffdb67a56b0b9cf92019e7)]

* Vagrant Cascadian submitted patches to fix reproducibility issues in [keyutils](https://issues.guix.gnu.org/55758) and [isl](https://issues.guix.gnu.org/55757) in [GNU Guix](https://www.gnu.org/software/guix/).
